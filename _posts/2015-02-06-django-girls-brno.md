---
layout: post
title: Django Girls Brno
author_name: Ondrej Sika
author_web: http://ondrejsika.com
author_email: ondrej@ondrejsika.com
author_twitter: http://twitter.com/ondrejsika
author_github: http://github.com/ondrejsika
---

Vcera se konala skvela akce, prvni Django Girls v Brne a ja mel tu cest byt jeji soucasti. Akce byla skvele pripravena a organizovana, ja jsem mel skvely team a vsechno se nam povedlo. Dokonce jsme zvladli trosku vic nez bylo v tutorialu. Atmosfera skvela, byl to super zazitek.


<blockquote class="twitter-tweet" lang="en"><p>The girls happily hacking away at the <a href="https://twitter.com/hashtag/django?src=hash">#django</a> apps! Go go <a href="https://twitter.com/hashtag/djangogirls?src=hash">#djangogirls</a> :-) <a href="http://t.co/j92saOY7pV">pic.twitter.com/j92saOY7pV</a></p>&mdash; Django Girls Brno (@DjangoGirlsBrno) <a href="https://twitter.com/DjangoGirlsBrno/status/563357196902477827">February 5, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


Meli jsme skupinky po trech, ja mel same Cesky, takze nam to hodne usnadnilo komunikaci. Holky tvorily jednoduchy blog, podle tutorialu, ktery je na vsech Djnago Girls stejny <http://tutorial.djangogirls.org/en/index.html>, a vysledkem jsou tyto stranky:

* <http://valsblog.herokuapp.com/>
* <http://petixkadjangogirl.herokuapp.com/>
* <http://janulas.herokuapp.com/>


Jeste par odkazu:

* [djangogirls.org/brno](http://djangogirls.org/brno/)
* [twitter.com/djangogirlsbrno](http://twitter.com/djangogirlsbrno/)

